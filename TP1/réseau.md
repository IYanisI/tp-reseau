# I. Exploration locale en solo

### (1) Affichez les infos des cartes réseau de votre PC.

>C:\user\yanis>ipconfig /all
>
>Get-NetIPAddress >-AddressFamily IPv4
>
>Get-NetIPAddress -AddressFamily IPv6
>
>Wifi:IPv6 ,F4-6D-3F-32-3F-5C, fe80::3489:8012:4c8e:f7ba%10
>Ethernet:IPv4 , >0A-00-27-00-00-04, 192.168.56.1

### (2) Affichez votre gateway.
>C:\user\yanis>(Get-WmiObject Win32_NetworkAdapterConfiguration | 
>Where-Object {$_.IPEnabled}).DefaultIPGateway
>
>10.33.51.254

### (3) Déterminer la MAC de la passerelle.
>C:\user\yanis>arp -a
>
>7c-5a-1c-cd-fd-a4

### (4) Trouvez comment afficher les informations sur une carte IP.

On le trouve sur propriété du matériel.


### (5)  Utilisez l'interface graphique de votre OS pour changer d'adresse IP.
Allez dans paramètre > réseau et internet > wifi > propriétés de wifi > attribution d'addresse IP. 

### (6) Expliquez pourquoi c'est possible de perdre son accès internet.
Si l'adresse internet change, il ne le reconnais plus ou l'adresse exist déja.


# II. Exploration locale en duo

### (1) Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau.

Panneau de configuration >
Réseau & Internet >
Centre de réseau et partage >
Modifier les paramètres de la carte > Ethernet > Propriétés > Protocol Internet Version 4.

### (2) Vérifier à l'aide d'une commande que votre IP a bien été changée.

> C:\user\yanis>ipconfig /all
>
>Carte Ethernet Ethernet :
>
>Adresse IPv6 :fe80::996b:ac7d:5cd9:1872%3
>
>Adresse IPv4 :10.10.10.3
>
>Masque de sous-réseau :255.255.255.0

### (3) Vérifier que les deux machines se joignent.

>C:\user\yanis>ping 10.10.10.4
>
>Envoi d'une requête 'ping'  10.10.10.4 avec 32 octets de données :
>
>Réponse de 10.10.10.4 : octets=32 temps<1ms TTL=128
>
>octets=32 temps=1ms TTL=128
>
>octets=32 temps=1ms TTL=128
>
>octets=32 temps=1ms TTL=128
>
>Statistiques Ping pour 10.10.10.4
>
>Paquets : envoyésv= 4, reçus = 4, perdus = 0 (perte 0%),
>
>durée approximative des boucles en millisecondes :
>
>Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms

### (4) Déterminer l'adresse MAC de votre correspondant.

>C:\user\yanis>arp -a
>
>10.10.10.4 ;
>c0-7c-d1-fd-50-90
>; dynamique

### (5) Créer un chat extrêmement simpliste sur le PC serveur.

>C:\Users\Unknown>
cd C:\Users\Unknown\Download\netcat-win32-1.12
>
>C:\Users\Unknown\Download\netcat-win32-1.12> .\nc64.exe -l -p 8888

Puis attendre le PC Client.

### (6) rejoindre le chat du PC serveur.

>C:\Users\yanis>
cd 
>
>C:\Users\yanis\Download\netcat-win32-1.12
>
>C:\Users\yanis\Download\netcat-win32-1.12> .\nc64.exe 10.10.10.4 8888

### (7) Visualiser la connexion en cours.

>C:\Users\Unknown> netstat -a -n -b 
>
>connexions actives
>
>TCP 10.10.10.4 : 8888
>10.10.10.3 : 62105
>ESTABLISHED

### (8) Pour aller un peu plus loin

>C:\Users\Unknown\Download\netcat-win32-1.12> nc64.exe -l -p 8888 -s 10.10.10.4

### (9) Activez et configurez votre firewall

Ouvrez le pare-feu Windows Defender>

Cliquez sur les règles de trafic entrant>

Trouvez la règle "Partage de fichiers et d'imprimantes (Demande Echo - ICMPv4-In)">

Autorisez les requêtes ping en cochant l'option "Allow inbound excho request.

### (10) Tester l'accès internet
