# I. First steps





# II. Setup Virtuel

### (1) Examinez le trafic dans Wireshark.

#### Déterminez si SSH utilise TCP ou UDP.                  
SSH utilise TCP

#### repérez le 3-Way Handshake à l'établissement de la connexion

| No. | Time | Source | Destination         | Protocol |Length | Info            |  
| ----- | -----| --------- | -------------------| ---- | ------------- | ---------------- |
| 1 | 0.000000| 10.4.1.11 | 10.4.1.1 | TCP | 66 | 50543 → 22 [SYN] Seq=0 Win=64240 Len=0 MSS=1460 WS=256 SACK_PERM|
| 2 | 0.000771| 10.4.1.11 | 10.4.1.1| TCP | 66 | 22 → 50543 [SYN,ACK] Seq=0 Ack=1 Win=64240 Len=0 MSS= 1460 SACK_PERM WS=128|
| 3 | 0.000924| 10.4.1.1 | 10.4.1.11| TCP | 54 | 50543 → 22 [ACK] Seq=1 Ack=1 Win=262656 Len=0|

#### repérez du trafic SSH

| No. | Time | Source | Destination         | Protocol |Length | Info            |  
| ----- | -----| --------- | -------------------| ---- | ------------- | ---------------- |
| 1| 0.000000| 10.4.1.1 |10.4.1.11 |SSH | 90 | Client: Encrypted packet (len=36)|
| 2 |0.001496| 10.4.1.11 | 10.4.1.1 | SSH | 90 | Server: Encrypted packet (len=36)|

#### repérez le FIN ACK à la fin d'une connexion

| No. | Time | Source | Destination         | Protocol |Length | Info            |  
| ----- | -----| --------- | -------------------| ---- | ------------- | ---------------- |
| 28| 10.135235| 10.4.1.1 |10.4.1.11 |TCP | 54 | 50801 → 22 [FIN,ACK] Seq=349 ACK=461 Win=8193 Len=0|
| 29 |10.142438| 10.4.1.11 | 10.4.1.1 | TCP | 60 | 50801 → 22 [FIN,ACK] Seq=461 ACK=350 Win= 501 Len=0|

### (2) Demandez aux OS.

>[yanis@localhost ~]$ sudo ss -tnp  
>
>State  |  Recv_Q | Send_Q | Local Address:Port | Peer Address:Port |Process |        
ESTAB  | 0 | 0 | 10.4.1.11:22 | 10.4.1.1:50234 | users:(("sshd",pid=1357,fd=4),("sshd",pid=1353,fd=4)) |

[SSH](ssh.pcapng)

### (3) Prouvez qu'il y a un accès internet.


>[yanis@localhost ~]$ ping 1.1.1.1
>PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.       
>64 bytes from 1.1.1.1: icmp_seq=1 ttl=55 time=23.5 ms      
>64 bytes from 1.1.1.1: icmp_seq=2 ttl=55 time=23.0 ms       
>--- 1.1.1.1 ping statistics ---        
>2 packets transmitted, 2 received, 0% packet loss, time 1003ms      
>rtt min/avg/max/mdev = 23.016/23.267/23.518/0.251 ms

### (4) Prouvez qu'il peut résoudre des noms de domaine publics.

>[yanis@localhost ~]$ ping youtube.com
PING youtube.com (142.250.179.110) 56(84) bytes of data.    
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=114 time=46.3 ms          
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=114 time=20.2 ms      
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=3 ttl=114 time=19.8 ms      
^C64 bytes from 142.250.179.110: icmp_seq=4 ttl=114 time=20.1 ms        
>
>--- youtube.com ping statistics ---        
4 packets transmitted, 4 received, 0% packet loss, time 15182ms     
rtt min/avg/max/mdev = 19.776/26.581/46.273/11.370 ms       

### (5) Installez le paquet nginx.

>[yanis@localhost ~]$ sudo dnf install nginx
>
>Rocky Linux 9 - BaseOS                                                                  4.8 kB/s | 4.1 kB     00:00        
>Rocky Linux 9 - AppStream                                                               8.4 kB/s | 4.5 kB     00:00     
Rocky Linux 9 - Extras                                                                  5.3 kB/s | 2.9 kB     00:00     
Dependencies resolved.      
========================================================================================================================>         
>
 >Package                         Architecture         Version                             Repository              Size    
>
>========================================================================================================================    
>
>
>Installing:     
 nginx                          
 x86_64               1:1.20.1-14.el9_2.1                 appstream                36 k     
Installing dependencies:        
 nginx-core                      x86_64               1:1.20.1-14.el9_2.1                 appstream               565 k     
 nginx-filesystem                noarch               1:1.20.1-14.el9_2.1                 appstream               8.5 k     
 rocky-logos-httpd               noarch               90.14-1.el9                         appstream                24 k     
>Transaction Summary 
>
>======================================================================================================================== 
>
>Install  4 Packages     
>
>Total download size: 634 k          
Installed size: 1.8 M       
Is this ok [y/N]: y     
Downloading Packages:       
(1/4): nginx-filesystem-1.20.1-14.el9_2.1.noarch.rpm                                     41 kB/s | 8.5 kB     00:00     
(2/4): rocky-logos-httpd-90.14-1.el9.noarch.rpm                                          98 kB/s |  24 kB     00:00     
(3/4): nginx-1.20.1-14.el9_2.1.x86_64.rpm                                               138 kB/s |  36 kB     00:00     
(4/4): nginx-core-1.20.1-14.el9_2.1.x86_64.rpm                                          945 kB/s | 565 kB     00:00  
>
>
>------------------------------------------------------------------------------------------------------------------------  
>
>
>Total                                                                          486 kB/s | 634 kB     00:01
>Running transaction check       
Transaction check succeeded.        
Running transaction test        
Transaction test succeeded.     
Running transaction     
 > Preparing        :                                                                                                1/1     
  Running scriptlet: nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                                                    1/4     
  Installing       : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                                                    1/4     
  Installing       : nginx-core-1:1.20.1-14.el9_2.1.x86_64                                                          2/4     
  Installing       : rocky-logos-httpd-90.14-1.el9.noarch                                                           3/4     
  Installing       : nginx-1:1.20.1-14.el9_2.1.x86_64                                                               4/4     
  Running scriptlet: nginx-1:1.20.1-14.el9_2.1.x86_64                                                               4/4     
  Verifying        : rocky-logos-httpd-90.14-1.el9.noarch                                                           1/4     
  Verifying        : nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                                                    2/4     
  Verifying        : nginx-1:1.20.1-14.el9_2.1.x86_64                                                               3/4     
  Verifying        : nginx-core-1:1.20.1-14.el9_2.1.x86_64                                                          4/4     
>
>Installed:      
  nginx-1:1.20.1-14.el9_2.1.x86_64                              nginx-core-1:1.20.1-14.el9_2.1.x86_64       
  nginx-filesystem-1:1.20.1-14.el9_2.1.noarch                   rocky-logos-httpd-90.14-1.el9.noarch        
>
>Complete!  

### (6) Créer le site web.

>[yanis@localhost ~]$ sudo mkdir /www
>
>[yanis@localhost ~]$ sudo mkdir /site_web_nul
>
>[yanis@localhost ~]$ cd /site_web_nul
>
>[yanis@localhost site_web_nul]$ nano index.html
>
>cd /
>
>[yanis@localhost /]$ sudo mv www/ ./var
>
>[yanis@localhost /]$ sudo mv site_web_nul/ ./var
>
>[yanis@localhost /]$ sudo mv site_web_nul/ ./www

### (7) Donner les bonnes permissions.

 >[yanis@localhost /]$ sudo chown -R nginx:nginx /var/www/site_web_nul

### (8) Créer un fichier de configuration NGINX pour notre site web.

>[yanis@localhost ~]$ cd /etc/nginx/conf.d/
>
>[yanis@localhost ~conf.d]$ sudo nano site_web_nul.conf

### (9) Démarrer le serveur web !

>[yanis@localhost ~]$ sudo systemctl status nginx
>nginx.service - The nginx HTTP and reverse proxy server         
Loaded: loaded (/usr/lib/system/nginx.service; disabled; preset: disabled)      
Active: active (running) since Web 2023-11-15 23:38:26 CET; 1min 29s ago        
Process: 1396 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)       
Process: 1397 ExecStartPre=/usr/sbin/nginx (code=exited, status0/SUCCESS)      
Main PID: 1399 (nginx)     
Tasks: 2 (limit: 4672)      
Memory: 3.3M        
CPU: 20ms       
CGroup: /system.slice/nginx.service     
    1399 "nginx: master process /usr/sbin/nginx"        
    1400 "nginx: worker process "
>
 >   Nov 15 23:38:26 localhost.localdomain systemd[1]: Starting The nginx HTTP and reverse proxy server...     
    Nov 15 23:38:26 localhost.localdomain snginx[1397]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok        
    Nov 15 23:38:26 localhost.localdomain snginx[1397]: nginx: configuration file /etc/nginx/nginx.conf             
    Nov 15 23:38:26 localhost.localdomain systemd[1]: Starting The nginx HTTP and reverse proxy server.


