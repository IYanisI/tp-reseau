# I. DHCP Client

### (1) Déterminer

#### l'adresse du serveur DHCP: 10.33.79.254   

#### l'heure à laquelle le bail DHCP obtenu: vendredi 27 octobre 2023 08:55:49 

#### l'heure à laquelle il va expirer:  samedi 28 octobre 2023 08:55:43

### (2) Capturer un échange DHCP

pour procéder a un échange DHCP complet, j'ai crée une ip fixe au wifi dans les paramettre, puis je l'ai déconecter à internet, j'ai remis l'ip dynamique et je l'ai reconecter à internet.

### (3) Analyser la capture Wireshark

les informations proposées au client parmi ces 4 trames est: le DHCP offer

[DHCP](DORA.pcapng)

# II. Serveur DHCP

### (1) Preuve de mise en place

## dhcp.tp4.b1

> [yanis@localhost ~]$ ping google.com      
Ping google.com (142.250.179.110) 56(84) bytes of data.     
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=115 time=60.9 ms       
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=115 time=46.9 ms       
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=3 ttl=115 time=60.1 ms  
^C64 bytes from 142.250.179.110: icmp_seq=4 ttl=115 time=52.7 ms
>
>--- google.com ping statistics ---      
4 packets transmitted, 4 received, 0% packet loss, time 15431       
rtt min/avg/max/mdev = 46.938/55.151/60.862/5.718 ms

## node2.tp4.b1

> [yanis@localhost ~]$ ping google.com      
Ping google.com (142.250.179.110) 56(84) bytes of data.     
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=115 time=71.4 ms       
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=115 time=47.3 ms       
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=3 ttl=115 time=55.3 ms  
^C64 bytes from 142.250.179.110: icmp_seq=4 ttl=115 time=43.5 ms
>
>--- google.com ping statistics ---      
4 packets transmitted, 4 received, 0% packet loss, time 15415       
rtt min/avg/max/mdev = 43.532/54.405/71.420/10.709 ms

> [yanis@localhost ~]$ traceroute 1.1.1.1       
traceroute to 1.1.1.1 (1.1.1.1), 30 hops max, 60 byte packets       
1 _gateway (10.4.1.254) 1.502 ms 0.982 ms 0.974 ms      
2 10.0.3.2 (10.0.3.2) 1.192 ms 1.469 ms 1.241 ms        
3 10.0.3.2 (10.0.3.2) 40.196 ms 65.932 ms 65.706 ms

 ### (2) Rendu

#### Toutes les commandes tapées pour monter le serveur DHCP.
##

 >[yanis@localhost ~]$ sudo dnf -y install dhcp-server 

>PS C:\Users\yanis> ssh yanis@10.4.1.253

 >[yanis@localhost ~]$ sudo nano /etc/dhcp/dhcpd.conf

 >[yanis@localhost ~]$ sudo systemctl start dhcpd
 ##

#### La commande pour afficher l'état du serveur.
##
 >[yanis@localhost ~]$ sudo systemctl status dhcpd
>
 >● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; preset: disabled)
     Active: active (running) since Sun 2023-11-05 13:46:26 CET; 52min ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
   Main PID: 11448 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 4672)
     Memory: 4.6M
        CPU: 8ms
     CGroup: /system.slice/dhcpd.service
             └─11448 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid
>
>Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Config file: /etc/dhcp/dhcpd.conf
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Database file: /var/lib/dhcpd/dhcpd.leases
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: PID file: /var/run/dhcpd.pid
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Source compiled to use binary-leases
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Wrote 0 leases to leases file.
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Listening on LPF/enp0s3/08:00:27:1c:9a:cb/10.4.1.0/24
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Sending on   LPF/enp0s3/08:00:27:1c:9a:cb/10.4.1.0/24
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Sending on   Socket/fallback/fallback-net
Nov 05 13:46:26 localhost.localdomain dhcpd[11448]: Server starting service.
Nov 05 13:46:26 localhost.localdomain systemd[1]: Started DHCPv4 Server Daemon.
##

#### La commande pour lire le fichier de configuration
##
>[yanis@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf     
>default-lease-time 600;     
>
>max-lease-time 7200;
>
>authoritative;
>
>subnet 10.4.1.0 netmask 255.255.255.0 {
>
>range dynamic-bootp 10.4.1.137 10.4.1.237;     
>}
##

### (3) Test.

> Subject: A start job for unit dhcpd.service has finished successfully     
Defined-By: systemd     
Support: https://access.redhat.com/support 
>
>A start job for unit dhcpd.service has finished successfully.
>
>The job identifier is 1487.
Nov 05 15:58:07 localhost.localdomain dhcpd[11448]: DHCPDISCOVER        
from 08:00:27:ae:bd:0a via enp0s3       
Nov 05 15:58:08 localhost.localdomain dhcpd[11448]: DHCPOFFER on 10.        
4.1.137 to 08:00:27:ae:bd:0a via enp0s3     
Nov 05 15:58:08 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 
10.4.1.137 (10.4.1.253) from 08:00:27:ae:bd:0a via enp0s3      
Nov 05 15:58:08 localhost.localdomain dhcpd[11448]: Wrote 1 leases to leases file.            
Nov 05 15:58:08 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3              
Nov 05 16:03:07 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 from 08:00:27:ae:bd:0a via enp0s3        
Nov 05 16:03:07 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:05:52 localhost.localdomain dhcpd[11448]: DHCPDISCOVER from 08:00:27:ae:bd:0a via enp0s3      
Nov 05 16:05:52 localhost.localdomain dhcpd[11448]: DHCPOFFER on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3     
Nov 05 16:05:52 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 (10.4.1.253) from 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:05:52 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:06:24 localhost.localdomain dhcpd[11448]: reuse_lease: lease age 32 (secs) under 25% threshold, reply with unaltered, existing lease for 10.4.1.137       
Nov 05 16:06:24 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 from 08:00:27:ae:bd:0a via enp0s3        
Nov 05 16:06:24 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:11:08 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 from 08:00:27:ae:bd:0a via enp0s3        
Nov 05 16:11:08 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:16:08 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 from 08:00:27:ae:bd:0a via enp0s3        
Nov 05 16:16:08 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:21:08 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 from 08:00:27:ae:bd:0a via enp0s3        
Nov 05 16:21:08 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:26:08 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 from 08:00:27:ae:bd:0a via enp0s3        
Nov 05 16:26:08 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:57:05 localhost.localdomain dhcpd[11448]: DHCPDISCOVER from 08:00:27:ae:bd:0a via enp0s3      
Nov 05 16:57:06 localhost.localdomain dhcpd[11448]: DHCPOFFER on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3             
Nov 05 16:57:06 localhost.localdomain dhcpd[11448]: DHCPREQUEST for 10.4.1.137 (10.4.1.253) from 08:00:27:ae:bd:0a via enp0s3       
Nov 05 16:57:06 localhost.localdomain dhcpd[11448]: DHCPACK on 10.4.1.137 to 08:00:27:ae:bd:0a via enp0s3       

### (4) Preuve.

#### Prouvez que node1.tp4.b1 a bien récupéré une IP dynamiquement.
##
>[yanis@localhost ~]$ ip a      
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000       
    link/ether 08:00:27:ae:bd:0a brd ff:ff:ff:ff:ff:ff      
    inet 10.4.1.137/24 brd 10.4.1.255 scope global dynamic      noprefixroute enp0s3        
       valid_lft 376sec preferred_lft 376sec        
    inet6 fe80::a00:27ff:feae:bd0a/64 scope link        
       valid_lft forever preferred_lft forever  
##    

#### Déterminer la date de création et la date d'expiration du bail.
##
>[yanis@localhost ~]$ 



##
#### déterminer l'adresse IP du serveur DHCP
##

[yanis@localhost ~]$ ip neigh show      
10.4.1.253 dev enp0s3 lladdr 08:00:27:1c:9a:cb STALE    
10.4.1.1 dev enp0s3 lladdr 0a:00:27:00:00:04 REACHABLE    

#### (5) Bail DHCP serveur

##
>[yanis@localhost ~]$ cat /var/lib/dhcpd/dhcpd.leases       
>lease 10.4.1.137 {     
  starts 0 2023/11/05 16:02:05;     
  ends 0 2023/11/05 16:12:05;       
  cltt 0 2023/11/05 16:02:05;       
  binding state active;     
  next binding state free;      
  rewind binding state free;        
  hardware ethernet 08:00:27:ae:bd:0a;      
  uid "\001\010\000'\256\275\012";      
}
##

#### (6) Nouvelle conf !
>[yanis@localhost ~]$ sudo cat /etc/dhcp/dhcpd.conf      
>option domain-name-servers 1.1.1.1;
>
>default-lease-time 21600;
>
>max-lease-time 21700;
>
>authoritative;
>
>subnet 10.4.1.0 netmask 255.255.255.0 {
>
>range dynamic-bootp 10.4.1.137 10.4.1.237;
>
>option broadcast-address 10.4.1.255;
>
>option routers 10.4.1.253;     
>}  

### (7) Test !

>[yanis@localhost /]$ sudo cat /etc/resolv.conf     
>#Generated by NetworkManager       
>nameserver 1.1.1.1

### (8) Capture Wireshark.

[wireshark](mon_fichier.pcap)