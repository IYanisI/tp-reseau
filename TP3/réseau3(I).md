# I. Echange ARP

### (1) Générer des requêtes ARP

## John

>[yanis@localhost /]$ ping 10.3.1.12    
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.    
64 bytes from 10.3.1.12: icmp=1 ttl=64 time=1.59 ms     
^C  
--- 10.3.1.12 ping statistics ---   
2 packets transmitted, 2 receved, 0% packet loss, time 1003ms   
rtt min/avg/max/mdev = 1.593/2.210/2.826/0.503 ms
>
> [yanis@localhost ]/$ ip neigh show     
10.3.1.12 dev enp0s3 lladdr 08:00:27:76:92:ad REACHABLE     
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:38 STALE
>
>[yanis@localhost /]$ ip neigh show 10.3.1.12   
10.3.1.12 dev enp0s3 lladdr 08:00:27:76:92:ad STALE
>
>[yanis@localhost /]$ ip a  


## Marcel

>[yanis@localhost /]$ ping 10.3.1.11   
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.    
64 bytes from 10.3.1.11: icmp=1 ttl=64 time=1.58 ms     
^C  
--- 10.3.1.11 ping statistics ---   
2 packets transmitted, 2 receved, 0% packet loss, time 1003ms   
rtt min/avg/max/mdev = 1.499/1.992/2.485/0.493 ms
>
> [yanis@localhost ]/$ ip neigh show     
10.3.1.11 dev enp0s3 lladdr 08:00:27:de:da:98 STALE     
>
>[yanis@localhost /]$ ip neigh show 10.3.1.11   
10.3.1.11 dev enp0s3 lladdr 08:00:27:de:da:98 STALE  
>
>[yanis@localhost /]$ ip a

# II. Analyse de trames

### (1) Analyse de trames

> [yanis@localhost /]$ sudo tcpdump -i enp0s3 -c 10 -w mon_fichier.pcap      
>
>[yanis@localhost /]$ sudo ip neigh flush all
>   
>[yanis@localhost /]$ ping 10.3.1.11

[ARP](./mon_premier.pcap)