# II. Routage.

## 1. Mise en place du routage.

### (1) Ajouter les routes statiques nécessaires pour que john et marcel puissent se ping.

#### marcel:

>[yanis@localhost]$ sudo ip route add 10.3.1.11 via 10.3.2.154 dev enp0s3

#### john:
>[yanis@localhost]$ sudo ip route add 10.3.2.12 via 10.3.1.254 dev enp0s3

##

>[yanis@localhost]$ ping 10.3.2.12  
>
>(10.3.2.12) 56(84) bytes of data.   
0.3.2.12: icmp_seq=1 ttl=63 time=2.97 ms  
0.3.2.12: icmp_seq=2 ttl=63 time=4.18 ms      
0.3.2.12: icmp_seq=3 ttl=63 time=5.68 ms      
0.3.2.12: icmp_seq=4 ttl=63 time=4.59 ms      
0.3.2.12: icmp_seq=5 ttl=63 time=2.51 ms      
--- 10.3.2.12 ping statistics ---   
5 packets transmitted, 5 receved, 0% packet loss, time 4012ms       
rtt min/avg/max/mdev = 2.507/3.984/5.677/1.138 ms

## 2. Analyse de trames.

### (1) Analyse des échanges ARP

>[yanis@localhost]$ sudo ip neigh flush all
>
>[yanis@localhost]$ping 10.3.2.12

| ordre | name | IP source | MAC source         | name |IP destination | MAC destination            |   request |
| ----- | -----| --------- | -------------------| ---- | ------------- | -------------------------- | ---------|
| 1 |`john`| 10.3.1.11 | 08:00:27:de:da:98  |`all`| XXX   | XXX          | Requête ARP |
| 2 | `router` | 10.3.2.154 | 08:00:27:a5:aa:fc | `john` | 10.3.1.11 | 08:00:27:de:da:27| Réponse ARP |
| 3 | `router` | 10.3.2.154 | 08:00:27:a5:aa:fc | `john` | 10.3.1.11 | 08:00:27:de:da:27| Requête ARP |
| 4 |`john`| 10.3.1.11 | 08:00:27:de:da:98  | `router` | 10.3.2.154 | 08:00:27:a5:aa:fc  | Réponse ARP |
| 5 | `router`| 10.3.2.154 | 08:00:27:a5:aa:fc |`all`| XXX | XXX | Requête ARP |
| 6 | `marcel`| 10.3.2.12 | 08:00:27:76:92:ad | `router`|10.3.2.154 | 08:00:27:a5:aa:fc | Réponse ARP |
| 7 | `marcel`| 10.3.2.12 | 08:00:27:76:92:ad | `router`|10.3.2.154 | 08:00:27:a5:aa:fc | Requête ARP |
| 8 | `router`| 10.3.2.154 | 08:00:27:a5:aa:fc |`marcel`| 10.3.2.12 | 08:00:27:76:92:ad | Réponse ARP |

## 3. Accès internet.

### (1) Donnez un accès internet à vos machines.

>[yanis@localhost ~]$ sudo firewall-cmd --add-masquerade  --permanent    
success
>
>[yanis@localhost ~]$ sudo firewall-cmd --reload    
success

### (2) Donnez un accès internet à vos machines.
 
 #### marcel:

>[yanis@localhost ~]$ sudo ip route add 1.1.1.1 via 10.3.2.154 dev enp0s3

#### john:

>[yanis@localhost ~]$ sudo ip route add 1.1.1.1 via 10.3.1.254 dev enp0s3

#### Resultat
>[yanis@localhost ~]$ ping 1.1.1.1

### (3) Analyse de trames.

[john](f1.pcap)

[marcel](f2.pcap)