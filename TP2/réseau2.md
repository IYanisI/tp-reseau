# I. Setup IP.

### (1) Mettre en place une configuration réseau fonctionnelle entre les deux machines.

## Ordinateur 1:

    IP = 10.10.10.1

    Mask = 255.255.255.252 

    Adresse réseau =10.10.10.0

    Broadcast = 10.10.10.3

## Ordinateur 2:

    IP = 10.10.10.2

    Mask = 255.255.255.252

    Adresse réseau =10.10.10.0

    Broadcast =10.10.10.3
##

Désolé, mais je n'ai pas fait de ligne de commande pour définir les adresses IP .

### (2)  Prouvez que la connexion est fonctionnelle entre les deux machines.
>ping 10.10.10.1
>
>Envoi d'une requête 'Ping' 10.10.10.1 avec 32 octets de donnés :
>
>Réponse de 10.10.10.1 : octets=32 temps<1ms TTL=128    
>Réponse de 10.10.10.1 : octets=32 temps=1 ms TTL=128   
>Réponse de 10.10.10.1 : octets=32 temps=1 ms TTL=128   
>Réponse de 10.10.10.1 : octets=32 temps=1 ms TTL=128
>
>Statistique Ping pour 10.10.10.1:  
>Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),   
>Durée approximative des boucles en millisecondes :     
>Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms

### (3) Wireshark analyse.

## ping envoyez.
 >22 82.889342 10.10.10.2 10.10.10.1 ICPM 74 Echo (ping)    
request id=0x0001, seq=37/9472, ttl=128 (reply in 23)

## pong retour.
>23 82.890051 10.10.10.1 10.10.10.2 ICPM 74 Echo (ping)    
reply id=0x0001, seq=37/9472, ttl=128 (request in 22)

[Ping.Pong](./ex1.5.pcapng)


# II. ARP my bro.

### (1) Check the ARP table

>arp -a
>
> adresse internet | adresse physique | type
>--- | --- | ---
>10.10.10.2 | c0-7c-d1-fb-50-90 | dynamique

>ipconfig /all  
>
>192.168.1.27    
F4-6D-3F-32-3F-5C

### (2) Manipuler la table ARP

>arp -d

Le changement qui est constaté, c'est que tout a était effacé, il ne reste plus que l'adresse 224.0.0.22 avec la MAC 01-00-5e-00-00-16.

Le ping pour 10.10.10.2 a réajouté deux adresse dans la table ARP.  
10.10.10.2 et 239.255.255.250.

### (3) Wireshark it

[ARP](./ex3.pcapng)

##
#### Pour la première trames.

 l'adresses source est: Pegatron_fb : 50 : 90   
 l'adresses de destination est: broadcast

 #### Et pour la deuxième trames.

l'adresses source est: sony_f4 : cc : ed    
 l'adresses de destination est: Pegatron_fb : 50 : 90 

 ##

 Chacune des fin de caractère sur sony ou Pegatron represente les fin de caractère des adresse MAC.

 # III. DHCP.

 ## (1) Wireshark it

 [DHCP](./ex5.pcapng)

 les 4 trames DHCP identifié sont:
 Discover,
 Offer,
 Request,
 et ACK

 Discover : 0.0.0.0 ; 255.255.255.255   
 Offer : 192.168.1.1 ; 192.168.1.27     
 Request : 0.0.0.0 ; 255.255.255.255    
 ACK : 192.196.1.1 ; 192.196.1.27