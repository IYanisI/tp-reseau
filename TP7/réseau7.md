# I. Fingerprint.

## (1) Effectuez une connexion SSH en vérifiant le fingerprint.

### Le message du serveur à la première connexion.


>PS C:\Users\yanis> ssh router       
The authenticity of host 'router (10.7.1.254)' can't be established.        
ED25519 key fingerprint is SHA256:MiIoeOp8wdNJEHAJrqGJSOccRm2g+l5O0cb/wGR3l+4.      
This key is not known by any other names        
Are you sure you want to continue connecting (yes/no/[fingerprint])? 
##    

### Commande qui me montre l'empreinte du serveur.

>[yanis@router ~]$ sudo ssh-keygen -l -f /etc/ssh/ssh_host_ed25519_key      
>[sudo] password for yanis:      
>256 SHA256:MiIoeOp8wdNJEHAJrqGJSOccRm2g+l5O0cb/wGR3l+4 /etc/ssh/ssh_host_ed25519_key.pub (ED25519)      

# 2. Conf serveur SSH.

## (2) Consulter l'état actuel.

### Vérifiez que le serveur SSH tourne actuellement sur le port 22/tcp et sur toutes les IPs de la machine.

>[yanis@router ssh]$ sudo ss -tulpn | grep :22    

| Netid | State |  Recv-Q | Send-Q | Local Address:Port | Peer Address:Port | Process |
| ----- | ----- | ------- | ------ | ------------------ | ----------------- | ------ |
tcp  | LISTEN |0  |    128     |     0.0.0.0:22    |    0.0.0.0:*   | users:(("sshd",pid=682,fd=3))|       
tcp  | LISTEN| 0   |   128    |         [::]:22     |      [::]:*  |  users:(("sshd",pid=682,fd=4)) |          

## (3) Modifier la configuration du serveur SSH.

>[yanis@router ssh]$ sudo nano sshd_config      
>#OpenBSD: sshd_config,v 1.104 2021/07/02 05:11:21 dtucker Exp
>
>#This is the sshd server system-wide configuration file.  See      
>#sshd_config(5) for more information.      
>
>#This sshd was compiled with PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin        
>
>#The strategy used for options in the default sshd_config shipped with     
#OpenSSH is to specify options with their default value where       
#possible, but leave them commented.  Uncommented options override the      
#default value.     
>
>#To modify the system-wide sshd configuration, create a  *.conf  file under        
>#/etc/ssh/sshd_config.d/  which will be automatically included below       
>Include /etc/ssh/sshd_config.d/*.conf      
>
>#If you want to change the port on a SELinux system, you have to tell      
#SELinux about this change.     
#semanage port -a -t ssh_port_t -p tcp #PORTNUMBER      
Port 25000              
#AddressFamily any      
ListenAddress 10.7.1.254        
#ListenAddress ::      

>[yanis@router ssh]$ sudo systemctl restart sshd                    
>          
>[yanis@router ssh]$ sudo ss -tulpn  

| Netid | State |  Recv-Q | Send-Q | Local Address:Port | Peer Address:Port | Process |
| ----- | ----- | ------- | ------ | ------------------ | ----------------- | ------ |
|  udp  | UNCONN |    0    |    0   |   127.0.0.1:323    |     0.0.0.0:*     | users:(("chronyd",pid=670,fd=5)) |
|  udp  | UNCONN |    0    |    0   |   [::1]:323        |       [::]:*      | users:(("chronyd",pid=670,fd=6)) |
|  tcp  |LISTEN |    0    |   128  |   10.7.1.254:25000 |     0.0.0.0:*     | users:(("sshd",pid=1706,fd=3)) |     

## Effectuer une connexion SSH sur le nouveau port.
### Depuis votre PC, connctez-vous en SSH au serveur.
>PS C:\Users\yanis> ssh router      
ssh: connect to host router port 22: Connection refused   

### Vu qu'on est passés du port standard (22/tcp) au port 25000, spécifier le port pour se connectée.

>PS C:\Users\yanis> ssh router -p 25000      
Last login: Fri Nov 24 11:00:42 2023 from 10.7.1.1      
   [yanis@router ~]$        

# 3. Connexion par clé.

## (1) Générer une paire de clés.

>C:\Users\yanis\\.ssh> cat .\id_rsa.pub     
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDTvfJHdXUBUrfOPr7KjsEosk2qZ/G4B3ZiiqJzGXdpSjlcY4FzYdegz1FGg4YnNV4JHkJStIzcTpVQxScI6iAPjcosPga+DBKNHEXjnCEZ8dgpncIhr1l/C7xDLSssj4L0Zmgu5S2lBYY86WA2achQxYGXhxaURgrJARzKF77WALaklYf56iKGGYbon5IWPfBoAaWiScQlaKbvWnknGOrjbWSHinF86WmYxMlULrYVYDE5cXjvT0iyZ1Cb2aIeUvT8+IBsnVGzu5s3bIG1MH4uuUFVMVuPShs8BEiPsQpPUWQ2lOOTOPedhpV2BBoyZ05pX+sVaAj2RwQldRCokjkZTi4+yCwqmyyZrljx0hutEmDeDLaxSzmTGTanOwkVs1jjhmB47skkgf2TqIW6d4yiP3KQuD719DSoCmM3tzILya32jYPXmL/rDh1FJh+J/rN/K6xcDEXhcrg2WhvzdngUsviwID7xOTuFsLQyG9Q4eKMUzz6EUyRklMapM1vTtUxJIv86nL6CEY6DctprMaOPb1nHrXLF5qSAyXHwlvQsjtE81qkIVlggznZ+SgDpQytjk33IZsx3VwvUdpgtotv1tEcqEuLiUCrImPdY7XTvUro1cjD8S33ibnlq0rA+NwACONissKXuaOb1bn3u8py2blOcIvSsBgRGRIGYyN/C6Q== yanis@BOOK-UBS8DTQGDI

### Consultez l'existence des clés si besoin        
>PS C:\Users\yanis\.ssh> ls      

####   Répertoire : C:\Users\yanis\.ssh        


|Mode        |         LastWriteTime      |   Length Name|
|----        |         -------------      |  ----------- |
|-a----    |    23/11/2023  |    10:10      |          3389 id_rsa|
|-a----    |    23/11/2023  |    10:10      |       748 id_rsa.pub|
|-a----    |    23/11/2023  |    12:26      |      819 known_hosts|
|-a----    |    23/11/2023  |    12:26      |   89 known_hosts.old|

## (2) Déposer la clé publique sur une VM.

>yanis@BOOK-UBS8DTQGDI MINGW64 ~     
$ ssh-copy-id john      
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/c/Users/yanis/.ssh/id_rsa.pub"      
The authenticity of host 'john (10.7.1.11)' can't be established.       
ED25519 key fingerprint is SHA256:MiIoeOp8wdNJEHAJrqGJSOccRm2g+l5O0cb/wGR3l+4.      
This host key is known by the following other names/addresses:      
    ~/.ssh/known_hosts:1: router        
    ~/.ssh/known_hosts:4: john.tp7.b1       
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes        
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed      
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys        
yanis@john's password:      
>
>Number of key(s) added: 1
>
>Now try logging into the machine, with:   "ssh 'john'"
and check to make sure that only the key(s) you wanted were added.

## (3) Connectez-vous en SSH à la machine.

>PS C:\> ssh john       
Last login: Thu Nov 23 12:11:23 2023 from 10.7.1.1      
[yanis@john ~]$     

## (4) Supprimer les clés sur la machine router.tp7.b1.

>sudo rm ssh_host_*

## (5) Regénérez les clés sur la machine router.tp7.b1.

>[yanis@localhost ssh]$ sudo ssh-keygen -A      
ssh-keygen: generating new host keys: RSA DSA ECDSA ED25519     

## (6) Tentez une nouvelle connexion au serveur.

>PS C:\> ssh router     
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @         
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!           
Someone could be eavesdropping on you right now (man-in-the-middle attack)!         
It is also possible that a host key has just been changed.          
The fingerprint for the ED25519 key sent by the remote host is          
SHA256:ZFxCb+WW+JM9NxuE+l4VCNXx32V8GirBHKJ9RjVM+C0.         
Please contact your system administrator.           
Add correct host key in C:\\Users\\yanis/.ssh/known_hosts to get rid of this message.           
Offending ECDSA key in C:\\Users\\yanis/.ssh/known_hosts:3          
Host key for router has changed and you have requested strict checking.         
Host key verification failed.  

#### Il faut supprimer la ligne qui pause problème dans known_hosts car il compare les clé.
