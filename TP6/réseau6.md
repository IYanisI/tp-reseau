# II. Setup

### (1) Le rendu a rendre.

### named.conf

>[yanis@localhost /]$ sudo cat /etc/named.conf       
>       
>       options {  
>     
>        listen-on port 53 { 127.0.0.1; any; };      
>        listen-on-v6 port 53 { ::1; };      
>        directory       "/var/named";       
>        dump-file       "/var/named/data/cache_dump.db";        
>        statistics-file "/var/named/data/named_stats.txt";      
>        memstatistics-file "/var/named/data/named_mem_stats.txt";       
>        secroots-file   "/var/named/data/named.secroots";       
>        recursing-file  "/var/named/data/named.recursing";      
>        allow-query     { localhost; any; };        
>        allow-query-cache { localhost; any; };      
>
>        /*     
>         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.        
>         - If you are building a RECURSIVE (caching) DNS server, you need to enable         
>        recursion.        
>         - If your recursive DNS server has a public IP address, you MUST enable access
>           control to limit queries to your legitimate users. Failing to do so will
>           cause your server to become part of large scale DNS amplification
>           attacks. Implementing BCP38 within your network would greatly
>     reduce such attack surface
>        */
>        recursion yes;
>
>        dnssec-validation yes;
>
>        managed-keys-directory "/var/named/dynamic";
>        geoip-directory "/usr/share/GeoIP";
>
>        pid-file "/run/named/named.pid";
>        session-keyfile "/run/named/session.key";
>
>     /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
 >       include "/etc/crypto-policies/back-ends/bind.config";
>       };
>
>       logging {      
>        channel default_debug {     
>                file "data/named.run";      
>                severity dynamic;       
>        };      
>       };      
>
>       zone "tp6.b1" IN {     
>        type master;        
>        file "tp6.b1.db";       
>        allow-update { none; };     
>        allow-query {any; };        
>        };      
>        zone "1.4.10.in-addr.arpa" IN {     
>        type master;        
>        file "tp6.b1.rev";      
>        allow-update { none; };     
>        allow-query {any; };        
>       };      
>
>       include "/etc/named.rfc1912.zones";     
>       include "/etc/named.root.key";
#
### tp6.b1.db 
>[yanis@localhost ~]$ sudo cat /var/named/tp6.b1.db     
$TTL 86400                                      
@ IN SOA dns.tp6.b1. admin.tp6.b1. (                 
    2019061800 ;Serial      
    3600 ;Refresh       
    1800 ;Retry     
    604800 ;Expire      
    86400 ;Minimum TTL      
)       
>
>; Infos sur le serveur DNS lui même (NS = NameServer)       
@ IN NS dns.tp6.b1.     
>
>; Enregistrements DNS pour faire correspondre des noms à des IPs        
dns       IN A 10.6.1.101       
john      IN A 10.6.1.11 
#
### tp6.b1.rev

>[yanis@localhost ~]$ sudo cat /var/named/tp6.b1.rev        
$TTL 86400      
@ IN SOA dns.tp6.b1. admin.tp6.b1. (        
    2019061800 ;Serial      
    3600 ;Refresh       
    1800 ;Retry     
    604800 ;Expire      
    86400 ;Minimum TTL      
)       
>
>; Infos sur le serveur DNS lui même (NS = NameServer)       
@ IN NS dns.tp6.b1.     
>
>; Reverse lookup        
101 IN PTR dns.tp6.b1.      
11 IN PTR john.tp6.b1.    

#
### Infos sur le service.

> named.service - Berkeley Internet Name Domain (DNS)        
     Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; preset: disabled)      
     Active: active (running) since Fri 2023-11-17 09:13:33 CET; 42min ago      
   Main PID: 700 (named)        
      Tasks: 5 (limit: 4604)        
     Memory: 21.5M      
        CPU: 89ms       
     CGroup: /system.slice/named.service        
             └─700 /usr/sbin/named -u named -c /etc/named.conf      
>
>Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 192.5.5.241#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 192.5.5.241#53       
Nov 17 09:13:33 localhost.localdomain named[700]: managed-keys-zone: Unable to fetch DNSKEY set '.': failure        
Nov 17 09:13:33 localhost.localdomain named[700]: resolver priming query complete       
Nov 17 09:13:33 localhost.localdomain named[700]: zone localhost.localdomain/IN: loaded serial 0        
Nov 17 09:13:33 localhost.localdomain named[700]: zone localhost/IN: loaded serial 0        
Nov 17 09:13:33 localhost.localdomain named[700]: all zones loaded      
Nov 17 09:13:33 localhost.localdomain named[700]: running       
Nov 17 09:13:33 localhost.localdomain systemd[1]: Started Berkeley Internet Name Domain (DNS).      
Nov 17 09:13:34 localhost.localdomain named[700]: listening on IPv4 interface enp0s3, 10.6.1.101#53     

#
### Logs en cas de probème.

>[yanis@localhost ~]$ sudo journalctl -xe -u named                  
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 199.7.83.42#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 199.7.83.42#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 192.58.128.30#53     
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 192.58.128.30#53     
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 192.36.148.17#53     
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 192.36.148.17#53     
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 193.0.14.129#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 193.0.14.129#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 199.9.14.201#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 199.9.14.201#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 199.7.91.13#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 199.7.91.13#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 202.12.27.33#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 202.12.27.33#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 198.97.190.53#53     
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 198.97.190.53#53     
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 192.112.36.4#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 192.112.36.4#53      
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 192.33.4.12#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 192.33.4.12#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 192.203.230.10#53            
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 192.203.230.10#53        
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './DNSKEY/IN': 192.5.5.241#53       
Nov 17 09:13:33 localhost.localdomain named[700]: network unreachable resolving './NS/IN': 192.5.5.241#53       
Nov 17 09:13:33 localhost.localdomain named[700]: managed-keys-zone: Unable to fetch DNSKEY set '.': failure        
Nov 17 09:13:33 localhost.localdomain named[700]: resolver priming query complete       
Nov 17 09:13:33 localhost.localdomain named[700]: zone localhost.localdomain/IN: loaded serial 0        
Nov 17 09:13:33 localhost.localdomain named[700]: zone localhost/IN: loaded serial 0        
Nov 17 09:13:33 localhost.localdomain named[700]: all zones loaded      
Nov 17 09:13:33 localhost.localdomain named[700]: running       
Nov 17 09:13:33 localhost.localdomain systemd[1]: Started Berkeley Internet Name Domain (DNS).      
Subject: A start job for unit named.service has finished successfully        
Defined-By: systemd      
Support: https://access.redhat.com/support       
>            
>A start job for unit named.service has finished successfully.        
>      
>The job identifier is 221.       
Nov 17 09:13:34 localhost.localdomain named[700]: listening on IPv4 interface enp0s3, 10.6.1.101#53    

## (2) Ouvrez le bon port dans le firewall.
###  Repéré sur quel port tourne le service.
>[yanis@localhost ~]$ sudo ss -ltupn       
>| Netid | State | Recv-Q | Send-Q | Local Address:Port | Peer Address:Port | Process |   
>| ----- | ----- | ------ | ------ | ------------------ | ----------------- | ------- |
>udp    |     UNCONN      | 0        |    0         |              10.6.1.101:53        |              0.0.0.0:*       |    users:(("named",pid=700,fd=6))
>udp       |  UNCONN   |   0         |   0            |            127.0.0.1:53        |              0.0.0.0:*          | users:(("named",pid=700,fd=16))
>tcp     |    LISTEN      | 0        |    10             |         10.6.1.101:53         |             0.0.0.0:*        |   users:(("named",pid=700,fd=24))
>tcp        | LISTEN     |  0         |   10            |           127.0.0.1:53       |               0.0.0.0:*         |  users:(("named",pid=700,fd=17))

#### Le service sur lequel il tourne est le port 53.
#
###
#### Ouvrez ce port dans le firewall de la machine.
> [yanis@localhost ~]$ sudo firewall-cmd --add-port=53/tcp --permanent        
success     

## (3) Configurez la machine pour qu'elle utilise le serveur DNS pour résoudre des noms.
>[yanis@localhost ~]$ cat /etc/resolv.conf       
nameserver 10.6.1.101

>[yanis@localhost ~]$ dig john.tp6.b1        
>
>; <<>> DiG 9.16.23-RH <<>> john.tp6.b1      
;; global options: +cmd             
;; Got answer:      
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 49825       
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0,        ADDITIONAL: 1     
>
>;; OPT PSEUDOSECTION:       
; EDNS: version: 0, flags:; udp: 1232       
; COOKIE: a3843acc8caf84110100000065574b9c8d03b521a690cf9c (good)       
;; QUESTION SECTION:        
;john.tp6.b1.                   IN      A       
>
>;; ANSWER SECTION:      
john.tp6.b1.            86400   IN      A       10.6.1.11       
>
>;; Query time: 7 msec       
;; SERVER: 10.6.1.101#53(10.6.1.101)        
;; WHEN: Fri Nov 17 12:16:40 CET 2023       
;; MSG SIZE  rcvd: 84       

##

>[yanis@localhost ~]$ dig dns.tp6.b1     
>
>; <<>> DiG 9.16.23-RH <<>> dns.tp6.b1       
;; global options: +cmd     
;; Got answer:      
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 30801       
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0,           ADDITIONAL: 1     
>
>;; OPT PSEUDOSECTION:       
; EDNS: version: 0, flags:; udp: 1232       
; COOKIE: 97a817d6a32af1230100000065574c036865fe2d7a516cd2       (good)       
;; QUESTION SECTION:        
;dns.tp6.b1.                    IN      A       
>
>;; ANSWER SECTION:      
dns.tp6.b1.             86400   IN      A       10.6.1.101      
>
>;; Query time: 4 msec       
;; SERVER: 10.6.1.101#53(10.6.1.101)        
;; WHEN: Fri Nov 17 12:18:23 CET 2023       
;; MSG SIZE  rcvd: 83           
##

>[yanis@localhost ~]$ dig google.com     
>
>; <<>> DiG 9.16.23-RH <<>> google.com       
;; global options: +cmd     
;; Got answer:      
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 18838       
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0,       ADDITIONAL: 1        
>
>;; OPT PSEUDOSECTION:       
; EDNS: version: 0, flags:; udp: 1232       
; COOKIE: 4647c890d05f78520100000065574c723272d833d615948d (good)       
;; QUESTION SECTION:        
;google.com.                    IN      A       
>        
>;; ANSWER SECTION:     
google.com.             300     IN      A       142.250.201.174     
>
>;; Query time: 32 msec      
;; SERVER: 10.6.1.101#53(10.6.1.101)        
;; WHEN: Fri Nov 17 12:20:14 CET 2023       
;; MSG SIZE  rcvd: 83       

## (4) Sur le PC, résoudre un nom de domaine en utilisant dns.tp6.b1 comme serveur DNS.
>PS C:\Users\yanis>  nslookup john.tp6.b1 10.6.1.101     
Serveur :   UnKnown     
Address:  10.6.1.101        
>
>Nom :    john.tp6.b1        
Address:  10.6.1.11     

## (5) Installer un serveur DHCP.

>[yanis@localhost ~]$ dnf -y install dhcp-server
>
>[yanis@localhost ~]$ sudo nano /etc/dhcp/dhcpd.conf        
>default-lease-time 600;
>
>max-lease-time 7200;
>
>authoritative;
>
>subnet 10.6.1.0 netmask 255.255.255.0 {
>
>range dynamic-bootp 10.6.1.13 10.6.1.37;       
>}

## (6) Test avec john.tp6.b1.
>[yanis@localhost ~]$ nano /etc/sysconfig/network-scripts/ifcfg-enp0s3      
>DEVICE=enp0s3
>
>BOOTPROTO=dhcp     
>ONBOOT=yes

>[yanis@localhost ~]$ ip a
>1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state        UNKNOWN group default qlen 1000     
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00       
    inet 127.0.0.1/8 scope host lo      
       valid_lft forever preferred_lft forever      
    inet6 ::1/128 scope host        
       valid_lft forever preferred_lft forever      
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc      fq_codel state UP group default qlen 1000      
    link/ether 08:00:27:20:64:b7 brd ff:ff:ff:ff:ff:ff      
    inet 10.6.1.13/24 brd 10.6.1.255 scope global dynamic       noprefixroute enp0s3        
         valid_lft 353sec preferred_lft 353sec      
    inet6 fe80::a00:27ff:fe20:64b7/64 scope link        
       valid_lft forever preferred_lft forever      

>[yanis@localhost ~]$ sudo cat /etc/resolv.conf
>nameserver 10.6.1.101

>[yanis@localhost ~]$ ip route show     
default via 10.6.1.254 dev enp0s3 proto static metric 100

>[yanis@localhost ~]$ ping 1.1.1.1      
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.            
64 bytes from 1.1.1.1: icmp_seq=1 ttl=62 time=12.1 ms       
64 bytes from 1.1.1.1: icmp_seq=2 ttl=62 time=8.32 ms       
64 bytes from 1.1.1.1: icmp_seq=3 ttl=62 time=7.12 ms       
64 bytes from 1.1.1.1: icmp_seq=4 ttl=62 time=8.52 ms       
^C      
--- 1.1.1.1 ping statistics ---     
4 packets transmitted, 4 received, 0% packet loss, time 3006ms      
rtt min/avg/max/mdev = 7.117/9.025/12.143/1.878 ms 

## (7) Requête web avec john.tp6.b1.

>[yanis@localhost ~]$ curl google.com               
>http-equiv="content-type" content="text/html;     
>charset=utf-8">     
>301 Moved      
>301 Moved      
>The document has moved      
>HREF="http://www.google.com/">here.

[DNS](mon_dns.pcap)

